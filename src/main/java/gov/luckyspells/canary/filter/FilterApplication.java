package gov.luckyspells.canary.filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.app.filter.processor.FilterProcessorConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(FilterProcessorConfiguration.class)
public class FilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilterApplication.class, args);
	}
}
